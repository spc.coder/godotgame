using Godot;
using System;

public class Lobby : Node
{
	// Declare member variables here. Examples:
	// private int a = 2;
	// private string b = "text";
	
	private const string IPADDRESS = "127.0.0.1";
	private const int PORT = 5000;
	
	private RichTextLabel ServerStatus { get; set; }

	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		ServerStatus = (RichTextLabel)GetNode("Status");
		ServerStatus.AddText("We ready.");
		GetTree().Connect("network_peer_connected", this, nameof(OnClientConnected));
		GetTree().Connect("network_peer_disconnected", this, nameof(OnClientDisconnected));
		GetTree().Connect("connected_to_server", this, nameof(OnServerConnected));
		GetTree().Connect("connection_failed", this, nameof(OnServerConnectionFailed));
		GetTree().Connect("server_disconnected", this, nameof(OnServerDisconnected));
		var client = new NetworkedMultiplayerENet();
		client.CreateClient(IPADDRESS, PORT);
		GetTree().NetworkPeer = client;
	}
	
	private void OnClientConnected(int id)
	{
		var statusText = (RichTextLabel)GetNode("Status");
		statusText.AddText($"hello {id}");
	}
	
	private void OnClientDisconnected(int id)
	{
		
	}
	
	private void OnServerConnected()
	{
		
	}
	
	private void OnServerConnectionFailed()
	{
		
	}
	
	private void OnServerDisconnected()
	{
		
	}

//  // Called every frame. 'delta' is the elapsed time since the previous frame.
//  public override void _Process(float delta)
//  {
//      
//  }
}
