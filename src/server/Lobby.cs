using Godot;
using System;

public class Node : Godot.Node
{
	// Declare member variables here. Examples:
	// private int a = 2;
	// private string b = "text";
	private const int PORT = 5000;
	private const int MAX_PLAYERS = 200;

	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		GetTree().Connect("network_peer_connected", this, "ClientConnected");
		GetTree().Connect("network_peer_disconnected", this, "ClientDisconnected");
		var server = new NetworkedMultiplayerENet();
		server.CreateServer(PORT, MAX_PLAYERS);
		GetTree().NetworkPeer = server;
	}
	
	private void ClientConnected(int id)
	{
		
	}
	
	private void ClientDisconnected(int id)
	{
		
	}

//  // Called every frame. 'delta' is the elapsed time since the previous frame.
//  public override void _Process(float delta)
//  {
//      
//  }
}
